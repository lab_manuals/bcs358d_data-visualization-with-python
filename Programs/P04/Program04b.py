import matplotlib.pyplot as plt
import numpy as np

# BRICS nations data (hypothetical)
countries = ['Brazil', 'Russia', 'India', 'China', 'South Africa']
population = [213993437, 145912025, 1393409038, 1444216107, 61608912]  # Population in 2021
per_capita_income = [9600, 11600, 2300, 11000, 6500]  # Per capita income in USD

# Scale the population for circle size
circle_size = [pop / 1000000 for pop in population]  # Scaling down for better visualization

# Assign different colors based on index
colors = np.arange(len(countries))

# Create a scatter plot with varying circle sizes and colors
scatter = plt.scatter(population, per_capita_income, s=circle_size, c=colors, cmap='viridis', alpha=0.7, label='BRICS Nations')

# Annotate each point with the country name
for i, country in enumerate(countries):
    plt.annotate(country, (population[i], per_capita_income[i]), textcoords="offset points", xytext=(0,5), ha='center')

# Add colorbar
plt.colorbar(scatter, label='Index')

# Add labels and title
plt.xlabel('Population')
plt.ylabel('Per Capita Income (USD)')
plt.title('Population vs Per Capita Income of BRICS Nations')

# Display the plot
plt.show()

